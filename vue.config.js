'use strict'



const basApiUrl = process.env.VUE_APP_BASEURL + 'api'
const pathRewrite = {}
pathRewrite['^' + basApiUrl] = '/'
const proxyObj = {}
proxyObj[basApiUrl] = {
    target: process.env.VUE_APP_SERVICEURL,
    ws: true,
    changeOrigin: true,
    pathRewrite: pathRewrite
}


module.exports = {
    publicPath: process.env.VUE_APP_BASEURL,
    outputDir: 'dist',
    devServer: {
        port: process.env.VUE_APP_PORT,
        proxy: proxyObj,
    },
    runtimeCompiler: true,
    productionSourceMap: false,
}