import Vue from 'vue'
import VueRouter from 'vue-router'
import router from './router.config.js'
import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import { axios_instance, dayjs } from './config.js'
import App from './App.vue'

Vue.use(VueRouter).use(Element);

Vue.prototype.$Qs = require('qs')
Vue.prototype.$axios = axios_instance
Vue.prototype.$dayjs = dayjs;

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
