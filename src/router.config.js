import VueRouter from 'vue-router'

const vueRouter = new VueRouter({
    mode: 'history',
    base: process.env.VUE_APP_BASEURL,
    routes: [
        {
            path: '/',
            meta: {
                auth: true
            },
            component: () => import('@/components/Index.vue'),
            children: [
                {
                    path: '/myDashboard',
                    meta: {
                        auth: true
                    },
                    component: () => import('@/components/MyDashboard.vue'),
                },
                {
                    path: '/myDevices',
                    meta: {
                        auth: true
                    },
                    component: () => import('@/components/MyDevices.vue'),
                }
            ]
        },
        {
            path: '/login',
            component: () => import('@/components/Login.vue'),

        },
    ]
})

vueRouter.beforeEach((to, from, next) => {
    let validator = typeof to.meta.auth == "undefined" || !to.meta.auth || sessionStorage.getItem('userId');
    let result = validator ? {} : {
        path: '/login'
    };
    next(result);
})


export default vueRouter