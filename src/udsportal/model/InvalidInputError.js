"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The InvalidInputError model module.
 * @module model/InvalidInputError
 * @version 1.0.0
 */
var InvalidInputError = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>InvalidInputError</code>.
   * @alias module:model/InvalidInputError
   */
  function InvalidInputError() {
    _classCallCheck(this, InvalidInputError);

    InvalidInputError.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(InvalidInputError, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>InvalidInputError</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/InvalidInputError} obj Optional instance to populate.
     * @return {module:model/InvalidInputError} The populated <code>InvalidInputError</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new InvalidInputError();

        if (data.hasOwnProperty('error')) {
          obj['error'] = _ApiClient["default"].convertToType(data['error'], 'String');
        }

        if (data.hasOwnProperty('error_description')) {
          obj['error_description'] = _ApiClient["default"].convertToType(data['error_description'], 'String');
        }
      }

      return obj;
    }
  }]);

  return InvalidInputError;
}();
/**
 * Can be used when credential or grant type is invalid. Values can be: unauthorized_client or unsupported_grant_type
 * @member {String} error
 */


InvalidInputError.prototype['error'] = undefined;
/**
 * @member {String} error_description
 */

InvalidInputError.prototype['error_description'] = undefined;
var _default = InvalidInputError;
exports["default"] = _default;