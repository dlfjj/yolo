"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _InvalidInputError = _interopRequireDefault(require("../model/InvalidInputError"));

var _TokenResponse = _interopRequireDefault(require("../model/TokenResponse"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* Default service.
* @module api/DefaultApi
* @version 1.0.0
*/
var DefaultApi = /*#__PURE__*/function () {
  /**
  * Constructs a new DefaultApi. 
  * @alias module:api/DefaultApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function DefaultApi(apiClient) {
    _classCallCheck(this, DefaultApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the getOrRefreshAuthorizationToken operation.
   * @callback module:api/DefaultApi~getOrRefreshAuthorizationTokenCallback
   * @param {String} error Error message, if any.
   * @param {module:model/TokenResponse} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Get or refresh authorization token
   * In an SSO with OAuth2, the client requests the access token with the authorization grant. This token should be used as an Authorization header of all requests to services that use OAuth2 security scheme type.<br><br>This endpoint returns a valid access token for 3 different grant types (`password`, `client_credentials` and `authorization_code`), or refresh a token when the grant type is `refresh_token`. For each grant type there are different mandatory fields. The other non-mandatory fields do not impact the result.<br>  **1) Mandatory fields when using Resource Owner Password Credentials:** <br> - grant_type=password;<br> - client_id; <br> - username;<br> - password<br>  **2) Mandatory fields when using Client Credentials:** <br> - grant_type=client_credentials;<br> - client_id;<br> - client_secret<br>  **3) Mandatory fields when using Authorization Code:** <br> - grant_type=authorization_code;<br> - client_id;<br> - redirect_uri; <br> - code<br> **4) Mandatory fields when using Refresh Token:** <br> - grant_type=refresh_token;<br> - client_id;<br> - client_secret;<br> - refresh_token.
   * @param {String} realm Realm ID created on the authentication server.
   * @param {Object} opts Optional parameters
   * @param {String} opts.grantType Refers to the way the application gets an access token. Acceptable values are `password`, `client_credentials`, `authorization_code` and `refresh_token`.
   * @param {String} opts.clientId Your application's client_id
   * @param {String} opts.username Username associated with the Realm to be used.
   * @param {String} opts.password Password associated with the username on the Realm to be used.
   * @param {String} opts.clientSecret Your application's client secret.
   * @param {String} opts.code Authorization code.
   * @param {String} opts.redirectUri URL user should be redirected after authenticated.
   * @param {String} opts.refreshToken Refresh token that is issued along with `access_token` in requests that get a token.
   * @param {module:api/DefaultApi~getOrRefreshAuthorizationTokenCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link module:model/TokenResponse}
   */


  _createClass(DefaultApi, [{
    key: "getOrRefreshAuthorizationToken",
    value: function getOrRefreshAuthorizationToken(realm, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'realm' is set

      if (realm === undefined || realm === null) {
        throw new Error("Missing the required parameter 'realm' when calling getOrRefreshAuthorizationToken");
      }

      var pathParams = {
        'realm': realm
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'grant_type': opts['grantType'],
        'client_id': opts['clientId'],
        'username': opts['username'],
        'password': opts['password'],
        'client_secret': opts['clientSecret'],
        'code': opts['code'],
        'redirect_uri': opts['redirectUri'],
        'refresh_token': opts['refreshToken']
      };
      var authNames = [];
      var contentTypes = ['application/x-www-form-urlencoded'];
      var accepts = ['application/json'];
      var returnType = _TokenResponse["default"];
      return this.apiClient.callApi('/auth/realms/{realm}/protocol/openid-connect/token', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the logout operation.
     * @callback module:api/DefaultApi~logoutCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Logout
     * After the user requests logout, the informed `redirect_uri` will be used to redirect the user back to the original service.
     * @param {String} realm Realm ID created on the authentication server.
     * @param {Object} opts Optional parameters
     * @param {String} opts.redirectUri Where the service redirects the user-agent after logout.
     * @param {module:api/DefaultApi~logoutCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "logout",
    value: function logout(realm, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'realm' is set

      if (realm === undefined || realm === null) {
        throw new Error("Missing the required parameter 'realm' when calling logout");
      }

      var pathParams = {
        'realm': realm
      };
      var queryParams = {
        'redirect_uri': opts['redirectUri']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = [];
      var contentTypes = [];
      var accepts = [];
      var returnType = null;
      return this.apiClient.callApi('/auth/realms/{realm}/protocol/openid-connect/logout', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the showLoginPage operation.
     * @callback module:api/DefaultApi~showLoginPageCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Show login page
     * Whenever a user needs to authenticate to a service through SSO, this service will build a SAML Authnrequest redirecting the user to this endpoint. After the user enters their credentials, the informed `redirect_uri` will be used to redirect the user back to the original service.
     * @param {String} realm Realm ID created on the authentication server.
     * @param {Object} opts Optional parameters
     * @param {String} opts.clientId Client ID created on the authentication server.
     * @param {String} opts.redirectUri Where the service redirects the user-agent after an authentication is granted.
     * @param {String} opts.state An opaque value, used to maintain state between the request and the callback. If this parameter is set in the request, then it is returned to the application as part of the `redirect_uri`
     * @param {String} opts.responseMode Informs the authorization server of the mechanism to be used for returning parameters from the Authorization Endpoint.
     * @param {String} opts.responseType Denotes the kind of credential that authorization server will return (`code` vs `token`). For this flow, the value must be `code`.
     * @param {String} opts.scope Specifies the level of access that the application is requesting, for this flow it must be `openid`.
     * @param {String} opts.nonce This parameter value needs to include per-session state and be unguessable to attackers. One method to achieve this for clients is to store a cryptographically random value and use a cryptographic hash of the value as the nonce parameter. In that case, the nonce in the returned ID Token is compared to the hash of the session to detect ID Token replay by third parties.
     * @param {String} opts.kcLocale Keycloak locale. Default is \"en\"
     * @param {module:api/DefaultApi~showLoginPageCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "showLoginPage",
    value: function showLoginPage(realm, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'realm' is set

      if (realm === undefined || realm === null) {
        throw new Error("Missing the required parameter 'realm' when calling showLoginPage");
      }

      var pathParams = {
        'realm': realm
      };
      var queryParams = {
        'client_id': opts['clientId'],
        'redirect_uri': opts['redirectUri'],
        'state': opts['state'],
        'response_mode': opts['responseMode'],
        'response_type': opts['responseType'],
        'scope': opts['scope'],
        'nonce': opts['nonce'],
        'kc_locale': opts['kcLocale']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = [];
      var contentTypes = [];
      var accepts = [];
      var returnType = null;
      return this.apiClient.callApi('/auth/realms/{realm}/protocol/openid-connect/auth', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return DefaultApi;
}();

exports["default"] = DefaultApi;