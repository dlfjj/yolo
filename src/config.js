import axios from 'axios'


const axios_instance = axios.create({
    baseURL: process.env.VUE_APP_BASEURL,
    timeout: 100000,
})

axios_instance.interceptors.response.use(function (response) {
    return response.data;
  }, function (error) {
    console.log("axios global response intercetors, ", error);
    console.log("axios global response intercetors, status:", error.request.status)
    return Promise.reject(error)
  });
axios_instance.defaults.withCredentials = true

const dayjs = require('dayjs')

export { axios_instance, dayjs }

