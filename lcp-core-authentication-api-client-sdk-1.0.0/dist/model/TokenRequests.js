"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The TokenRequests model module.
 * @module model/TokenRequests
 * @version 1.0.0
 */
var TokenRequests = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>TokenRequests</code>.
   * @alias module:model/TokenRequests
   */
  function TokenRequests() {
    _classCallCheck(this, TokenRequests);

    TokenRequests.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(TokenRequests, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>TokenRequests</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/TokenRequests} obj Optional instance to populate.
     * @return {module:model/TokenRequests} The populated <code>TokenRequests</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new TokenRequests();

        if (data.hasOwnProperty('grant_type')) {
          obj['grant_type'] = _ApiClient["default"].convertToType(data['grant_type'], 'String');
        }

        if (data.hasOwnProperty('client_id')) {
          obj['client_id'] = _ApiClient["default"].convertToType(data['client_id'], 'String');
        }

        if (data.hasOwnProperty('username')) {
          obj['username'] = _ApiClient["default"].convertToType(data['username'], 'String');
        }

        if (data.hasOwnProperty('password')) {
          obj['password'] = _ApiClient["default"].convertToType(data['password'], 'String');
        }

        if (data.hasOwnProperty('client_secret')) {
          obj['client_secret'] = _ApiClient["default"].convertToType(data['client_secret'], 'String');
        }

        if (data.hasOwnProperty('code')) {
          obj['code'] = _ApiClient["default"].convertToType(data['code'], 'String');
        }

        if (data.hasOwnProperty('redirect_uri')) {
          obj['redirect_uri'] = _ApiClient["default"].convertToType(data['redirect_uri'], 'String');
        }

        if (data.hasOwnProperty('refresh_token')) {
          obj['refresh_token'] = _ApiClient["default"].convertToType(data['refresh_token'], 'String');
        }
      }

      return obj;
    }
  }]);

  return TokenRequests;
}();
/**
 * Refers to the way the application gets an access token. Acceptable values are `password`, `client_credentials`, `authorization_code` and `refresh_token`.
 * @member {String} grant_type
 */


TokenRequests.prototype['grant_type'] = undefined;
/**
 * Your application's client_id
 * @member {String} client_id
 */

TokenRequests.prototype['client_id'] = undefined;
/**
 * Username associated with the Realm to be used.
 * @member {String} username
 */

TokenRequests.prototype['username'] = undefined;
/**
 * Password associated with the username on the Realm to be used.
 * @member {String} password
 */

TokenRequests.prototype['password'] = undefined;
/**
 * Your application's client secret.
 * @member {String} client_secret
 */

TokenRequests.prototype['client_secret'] = undefined;
/**
 * Authorization code.
 * @member {String} code
 */

TokenRequests.prototype['code'] = undefined;
/**
 * URL user should be redirected after authenticated.
 * @member {String} redirect_uri
 */

TokenRequests.prototype['redirect_uri'] = undefined;
/**
 * Refresh token that is issued along with `access_token` in requests that get a token.
 * @member {String} refresh_token
 */

TokenRequests.prototype['refresh_token'] = undefined;
var _default = TokenRequests;
exports["default"] = _default;